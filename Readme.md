# Cuadrado Mágico

Escriba una función que muestre un cuadrado mágico de orden impar `n`
comprendido entre `3` y `11`, valor que se recibe por parámetro. El cuadrado
mágico se compone de números enteros comprendidos entre `1` y `n`. La suma de
los números por fila, columna y diagonal son iguales. Ejemplo:

```text
8 | 1 | 6 |
3 | 5 | 7 |
4 | 9 | 2 |
```

Un método para generar el cuadrado mágico consiste en ubicar el número 1 en el
centro de la primera fila, el número siguiente en la casilla situada por encima
y a la derecha y así sucesivamente. En caso de que el número generado caiga en
una casilla ocupada, se elige la casilla que se encuentre debajo del número que
acaba de ser ubicado.

## Refinamiento

### Función: `cuadMag`

Recibe orden de la matriz. Asigna valores para que la suma de los números por
fila, columna y diagonal sean iguales.

#### Entradas:

- `n`: orden de la matriz.

#### Salidas: 

- Ningua (impresión en pantalla).

#### Pseudo-código

1. Si `n` es igual a `9`:
   1. Declaro matriz `cuad` de tamaño 3x3 e inicializo a ceros.
   1. Declaro posición de fila `fil` y asigno valor `0`.
   1. Declaro posición de columna `col` y asigno valor `1`.
   1. Declaro número a ubicar por casilla `num` y asigno valor `1`.
   1. Asigno `1` a posición inicial de de matriz `cuad`.
   1. Mientras `num` sea menor igual a `n`:
      1. Declaro `nFil` y asigno `2` si `fil - 1` es negativo, de lo contrario
         asigno `fil - 1`.
      1. Declaro `nCol` y asigno módulo 3 de col + 1.
      1. Si valor en posición `[nFil][nCol]` de matriz `cuad` es diferente a
         `0`:
         1. Asigno a `nCol` el valor de `col`.
         1. Asigno a `nFil` módulo 3 de `fil + 1`.
      1. Asigno valor de `nCol` a `col`.
      1. Asigno valor de `nFil` a `fil`.
      1. Incremento num en 1 y asigno valor a posición `[fil][col]` de matriz
         `cuad`.
    1. Imprimo contenidos de matriz `cuad`.
1. De lo contrario:
   1. Imprimir error: "n no es impar no primo entre 3 y 11"
