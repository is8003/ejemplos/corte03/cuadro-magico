#include <stdio.h>
#include <stdlib.h>

void cuadMag(unsigned n);

int main(void){
	cuadMag(9);
	exit(EXIT_SUCCESS);
}


void cuadMag(unsigned n){
	if (n == 9){
		unsigned cuad[3][3] = {0};

		int fil = 0;
		int col = 1;
		unsigned num = 1;

		cuad[fil][col] = num;

		while (num < n){
			int nFil = (fil - 1 < 0) ? 2 : fil - 1;
			int nCol = (col + 1 ) % 3;

			if (cuad[nFil][nCol] != 0){
				nCol = col;
				nFil = (fil + 1) % 3;
			}

			col = nCol;
			fil = nFil;
			cuad[fil][col] = ++num;
		}


		for (unsigned i = 0; i < 9; i++){
			printf("%2u |", cuad[i/3][i%3]);
			if ((i + 1) % 3 == 0) puts("");
		}
	}
	else{
		fprintf(stderr, "error: %u no es impar no primo entre 3 y 11\n", n);
	}
}
